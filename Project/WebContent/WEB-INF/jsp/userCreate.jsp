<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <!-- レスポンシブWebデザインを使用 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>新入部員登録</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- オリジナルCSS読み込み -->
    <link href="./CSS/bootstrap.css" rel="stylesheet">
    <link href="./CSS/common.css" rel="stylesheet">

</head>
<body>

    <!-- ヘッダ -->
    <header>
    <nav class="navbar navbar-light bg-danger border sticky-top">
        <a class="btn btn-outline-primary" href="UserListServlet">戻る</a>
        <a class="navbar-brand mx-auto font-weight-bold" href="UserListServlet">焔原学園高校弓道部</a>
        <nav>${userInfo.name} さん　</nav>
        <a class="btn btn-outline-primary" href="LogoutServlet">ログアウト</a>
    </nav>
    </header>

    <!-- エラーがある場合のみ表示 -->
    <c:if test="${errMsg != null}">
	    <div class="alert alert-warning mx-auto col-9 mt-3" role="alert">
		  ${errMsg}
		</div>
	</c:if>

    <!-- 登録フォーム -->
    <div class="form-area border border-primary col-sm-8 mx-auto">
        <form method="post" action="UserCreateServlet" class="form-horizontal">
            <div class="form-group form-row">
                <label class="col-sm-3 col-form-label">ログインID</label>
                <input type="text" class="form-control col-sm-8" id="loginId" name="newLoginId" required autofocus>
            </div>

            <div class="form-group form-row">
                <label class="col-sm-3 col-form-label">ユーザー名</label>
                <input type="text" class="form-control col-sm-8" id="userName" name="newUserName" required>
            </div>

            <div class="form-group form-row">
                <label class="col-sm-3 col-form-label">生年月日</label>
                <input type="date" class="form-control col-sm-8" id="birthDate" name="newBirthDate" required>
            </div>

            <div class="form-group form-row">
                <label class="col-sm-3 col-form-label">パスワード</label>
                <input type="password" class="form-control col-sm-8" id="password" name="newPassword" required>
            </div>

            <div class="form-group form-row">
                <label class="col-sm-3 col-form-label">パスワード(確認)</label>
                <input type="password" class="form-control col-sm-8" id="password-confirm" name="newConfirmPassword" required>
            </div>

            <div class="col-sm-4 mx-auto pb-3">
                <button type="submit" class="btn btn-primary btn-block form-submit">登録</button>
            </div>
        </form>
    </div>

</body>
</html>
