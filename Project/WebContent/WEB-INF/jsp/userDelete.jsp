<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <!-- レスポンシブWebデザインを使用 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>部員情報削除</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- オリジナルCSS読み込み -->
    <link href="./CSS/bootstrap.css" rel="stylesheet">
    <link href="./CSS/common.css" rel="stylesheet">

</head>
<body>

    <!-- ヘッダ -->
    <header>
    <nav class="navbar navbar-light bg-danger border sticky-top">
        <a class="btn btn-outline-primary" href="UserListServlet">戻る</a>
        <a class="navbar-brand mx-auto font-weight-bold" href="UserListServlet">焔原学園高校弓道部</a>
        <nav>${userInfo.name} さん　</nav>
        <a class="btn btn-outline-primary" href="LogoutServlet">ログアウト</a>
    </nav>
    </header>

    <!-- 削除確認 -->
    <div class="container mt-5">
        <div class="delete-area">
            <p class="font-weight-bold">ログインID：${deleteUser.loginId}</p>
            <p class="font-weight-bold">　ユーザ名：${deleteUser.name}</p>
            <p>を本当に削除してよろしいですか？</p>
          <div class="row justify-content-around">
            <div class="col-sm-4">
              <a href="UserListServlet" class="btn btn-light btn-block">いいえ</a>
            </div>
            <div class="col-sm-4">
              <a href ="UserDeleteServlet?id=${deleteUser.id}" class="btn btn-primary btn-block">はい</a>
            </div>
          </div>
        </div>
    </div>

</body>
</html>
