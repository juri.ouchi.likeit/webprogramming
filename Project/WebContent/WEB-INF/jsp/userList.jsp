<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <!-- レスポンシブWebデザインを使用 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>弓道部員一覧</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

    <!-- オリジナルCSS読み込み -->
    <link href="./CSS/bootstrap.css" rel="stylesheet">
    <link href="./CSS/common.css" rel="stylesheet">

</head>
<body>

    <!-- ヘッダ -->
    <header>
    <nav class="navbar navbar-light bg-danger border sticky-top">
    	<!-- 新規登録ボタンの表示制御 -->
    	<c:if test="${userInfo.loginId.equals('admin')}">
	    	<a class="btn btn-primary" href="UserCreateServlet">新規登録</a>
		</c:if>
        <a class="navbar-brand mx-auto font-weight-bold" href="UserListServlet">焔原学園高校弓道部</a>
        <nav>${userInfo.name} さん　</nav>
        <a class="btn btn-outline-primary" href="LogoutServlet">ログアウト</a>
    </nav>
    </header>


    <!-- 検索フォーム -->
    <div class="form-area border border-primary col-sm-8 mx-auto">
        <form method="post" action="UserListServlet" class="form-horizontal">

            <div class="form-group form-row">
                <label class="col-sm-2 col-form-label">ログインID</label>
                <input type="text" class="form-control col-sm-7" id="loginId" name="loginId">
            </div>

            <div class="form-group form-row">
                <label class="col-sm-2 col-form-label">ユーザー名</label>
                <input type="text" class="form-control col-sm-7" id="userName" name="userName">
            </div>

            <div class="form-group form-row">
                <label class="col-sm-2 col-form-label">生年月日</label>
                <div class="form-row col-sm-8 pl-0">
                    <div class="col-sm-5">
                        <input type="date" id="date-start" class="form-control" name="startBirthDate">
                    </div>
                    <div class="col-sm-1 mx-auto align-middle">
                        ～
                    </div>
                    <div class="col-sm-5">
                        <input type="date" id="date-end" class="form-control" name="endBirthDate">
                    </div>
                </div>

                <div class="col-sm-2">
                <button type="submit" class="btn btn-primary form-submit">検索</button>
                </div>
            </div>
        </form>
    </div>


    <!-- 一覧 -->
    <div class="table-responsive col-12">
      <table class="table table-striped">
        <thead>
          <tr class="table-primary">
            <th>ログインID</th>
            <th>ユーザ名</th>
            <th>生年月日</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
        <c:forEach var="user" items="${userList}">
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <!-- TODO ログインボタンの表示制御を行う -->
                     <td>
                       <!-- 詳細 -->
                       <a class="btn btn-info" href="UserDetailServlet?id=${user.id}"><i class="fas fa-search-plus fa-lg"></i></a>

					   <!-- 更新と削除 -->
					   <c:choose>
						  <c:when test="${userInfo.loginId.equals('admin')}">
						  	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}"><i class="fas fa-pen fa-lg"></i></a>
						  	<a class="btn btn-danger" href ="DeleteCheckServlet?id=${user.id}"><i class="fas fa-eraser fa-lg"></i></a>
						  </c:when>
						  <c:when test="${userInfo.loginId.equals(user.loginId)}">
						  	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}"><i class="fas fa-pen fa-lg"></i></a>
                       		<a class="btn btn-danger" href ="DeleteCheckServlet?id=${user.id}"><i class="fas fa-eraser fa-lg"></i></a>
						  </c:when>
						  <c:otherwise>
						  	<!--
						  	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}" hidden><i class="fas fa-pen fa-lg"></i></a>
                       		<a class="btn btn-danger" href ="DeleteCheckServlet?id=${user.id}" hidden><i class="fas fa-eraser fa-lg"></i></a>
						     -->
						  </c:otherwise>
					   </c:choose>

                     </td>
                   </tr>
        </c:forEach>

        <!--
          <tr>
            <td>id0001</td>
            <td>田中太郎</td>
            <td>1989年04月20日</td>
            <td>
              <a class="btn btn-info" href="userDetail.html"><i class="fas fa-search-plus fa-lg"></i></a>
              <a class="btn btn-success" href="userUpdate.html"><i class="fas fa-pen fa-lg"></i></a>
              <a class="btn btn-danger" href="userDelete.html"><i class="fas fa-eraser fa-lg"></i></a>
            </td>
          </tr>
          <tr>
            <td>id0001</td>
            <td>田中太郎</td>
            <td>1989年04月20日</td>
            <td>
              <a class="btn btn-info" href="userDetail.html"><i class="fas fa-search-plus fa-lg"></i></a>
              <a class="btn btn-success" href="userUpdate.html"><i class="fas fa-pen fa-lg"></i></a>
              <a class="btn btn-danger" href="userDelete.html"><i class="fas fa-eraser fa-lg"></i></a>
            </td>
          </tr>
          -->

        </tbody>
      </table>
    </div>

</body>
</html>