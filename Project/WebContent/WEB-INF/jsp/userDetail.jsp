<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <!-- レスポンシブWebデザインを使用 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>部員詳細</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- オリジナルCSS読み込み -->
    <link href="./CSS/bootstrap.css" rel="stylesheet">
    <link href="./CSS/common.css" rel="stylesheet">

</head>
<body>

    <!-- ヘッダ -->
    <header>
    <nav class="navbar navbar-light bg-danger border sticky-top">
        <a class="btn btn-outline-primary" href="UserListServlet">戻る</a>
        <a class="navbar-brand mx-auto font-weight-bold" href="UserListServlet">焔原学園高校弓道部</a>
        <nav>${userInfo.name} さん　</nav>
        <a class="btn btn-outline-primary" href="LogoutServlet">ログアウト</a>
    </nav>
    </header>

    <!-- 詳細情報表示 -->
    <div class="table-responsive border border-primary mx-auto col-6">
    <table class="table table-borderless ">
        <tbody>
            <tr><td>ログインID</td><td>${userDetail.loginId}</td></tr>
            <tr><td>ユーザ名</td><td>${userDetail.name}</td></tr>
            <tr><td>生年月日</td><td>${userDetail.birthDate}</td></tr>
            <tr><td>登録日時</td><td>${userDetail.createDate}</td></tr>
            <tr><td>更新日時</td><td>${userDetail.updateDate}</td></tr>
        </tbody>
    </table>

        <div class="col-sm-4 mx-auto pb-3">
            <a class="btn btn-primary btn-block" href="UserListServlet">確認</a>
        </div>
    </div>

</body>
</html>
