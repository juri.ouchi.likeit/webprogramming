package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// ユーザが自分のアカウントを削除した場合、ログイン時に保存したセッション内のユーザ情報を削除
		// 削除対象アカとログイン中アカの同一判定は、DBからの情報削除前にやらないと意味がないよ！！

		// セッションからログインIDを取り出す
		UserDao userDao = new UserDao();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		String loginId = user.getLoginId();

		// checkDuplicateメソッドで管理者避けたんだから通り道作らないと当然エラーになるね。
		if (!loginId.equals("admin")) {
		// DBからログインIDに紐づくIDを検索
		User loginUser = userDao.checkDuplicate(loginId);
		// IDを数値として比較し同一判定
			if (Integer.parseInt(id) == loginUser.getId()) {
				session.removeAttribute("userInfo");
			}
		}
		// idを引数にして、Daoの削除メソッドを実行＝DBからユーザ情報を削除
		userDao.deleteUser(id);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
