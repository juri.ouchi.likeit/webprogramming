package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("newLoginId");
		String userName = request.getParameter("newUserName");
		String password = request.getParameter("newPassword");
		String conPassword = request.getParameter("newConfirmPassword");
		// 生年月日はString型からSQLのDate型に変換
		String strDate = request.getParameter("newBirthDate");
		Date birthDate = Date.valueOf(strDate);


		// リクエストパラメータの入力項目を引数に渡して、Daoの重複確認メソッドを実行
		UserDao userDao = new UserDao();
		User user = userDao.checkDuplicate(loginId);

		if (loginId.length() == 0 || userName.length() == 0 || strDate == null || password.length() == 0) {
			/** 1つでも未入力の項目がある場合 **/
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "全ての項目を入力してください。");

			// 新規登録JSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;

		} else 	if (!password.equals(conPassword)) {
			/** パスワードとパスワード(確認)が一致しない場合 **/
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "確認用のパスワードが一致しません。");

			// 新規登録JSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;

		} else if (user != null) {
			/** テーブルに既に登録されているログインIDが見つかった場合 **/
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力されたログインIDは既に登録されています。");

			// 新規登録JSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;

		} else {
			// リクエストパラメータの入力項目を引数に渡して、Daoの登録用メソッドを実行
			userDao.createUser(loginId, userName, birthDate, password);

			/** 登録に成功した場合 **/
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}

	}

}
