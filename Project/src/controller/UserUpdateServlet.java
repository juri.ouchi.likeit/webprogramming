package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User userDetail = userDao.findDetail(id);

		// ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("userDetail", userDetail);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
        String loginId = request.getParameter("loginId");
		String userName = request.getParameter("newUserName");
		String password = request.getParameter("newPassword");
		String conPassword = request.getParameter("newConfirmPassword");
		// 生年月日はString型からSQLのDate型に変換
		String strDate = request.getParameter("newBirthDate");
		Date birthDate = Date.valueOf(strDate);

	 	if (!password.equals(conPassword)) {
			/** パスワードとパスワード(確認)が一致しない場合 **/
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "確認用のパスワードが一致しません。");

			// 更新JSPにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;

	 	} else if (userName.length() == 0 || strDate == null) {
			/** パスワード以外に未入力の項目がある場合 **/
	 		// リクエストスコープにエラーメッセージをセット
	 		request.setAttribute("errMsg", "全ての項目を入力してください。");

	 		// 更新JSPにフォワード
	 		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	 		dispatcher.forward(request, response);
	 		return;

	 	} else if (password.length() == 0) {
			/** パスワードが空欄の場合 **/
	 		// リクエストパラメータのパスワード以外の入力項目を引数に渡して、Daoの更新用メソッドを実行
	 		UserDao userDao = new UserDao();
	 		userDao.updateUser(loginId, userName, birthDate);

	 		/** 更新に成功 **/
	 		// ユーザ一覧のサーブレットにリダイレクト
	 		response.sendRedirect("UserListServlet");

		} else {
			// リクエストパラメータの入力項目を引数に渡して、Daoの更新用メソッドを実行
			UserDao userDao = new UserDao();
			userDao.updateUser(loginId, userName, birthDate, password);

			/** 更新に成功 **/
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}

	}

}
