CREATE DATABASE user_management DEFAULT CHARACTER SET utf8;

USE user_management;

CREATE TABLE user( 
	id SERIAL,
	login_id varchar(255) UNIQUE NOT NULL,
	name varchar(255) NOT NULL,
	birth_date date NOT NULL,
	password varchar(255) NOT NULL,
	create_date datetime NOT NULL,
	update_date datetime NOT NULL
);

INSERT INTO user (login_id, name, birth_date, password, create_date, update_date) VALUES ('admin', '�Ǘ���', '1982-07-07', 'password', '2019-10-21', '2019-10-21');
